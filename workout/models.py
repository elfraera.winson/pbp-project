from django.db import models
# Create your models here.

class Video(models.Model):
    vidTitle = models.CharField(max_length=100)
    vidAdded = models.DateTimeField(auto_now_add=True)
    vidUrl = models.CharField(max_length=100)
    # vidFile = models.FileField(upload_to="workout/%y", blank=True)
    def __str__(self):
        return self.vidTitle
    