from django import forms
from .models import Comment
RATINGS=[
    ('1', '1'),
    ('2', '2'),
    ('3', '3'),
    ('4', '4'),
    ('5', '5'),
]

class CommentForm(forms.ModelForm):
    class Meta:
        model = Comment
        fields = ["id_number", "name","rating","comment"]
    input_attrs = [{
		'type' : 'text',
		'placeholder' : 'Write your name',
        'class' : 'form-control'
	},{ 'class': 'form-control'  
    },{
        'type' : 'text',
        'placeholder' : 'Write the comment here...',
        'class' : 'form-control'
    }
    ]

    id_number = forms.IntegerField(required=False)   
    name = forms.CharField(label='', required=True, max_length=30, widget=forms.TextInput(attrs=input_attrs[0]))
    rating = forms.CharField(label='', required=True, widget=forms.Select(input_attrs[1], choices=RATINGS))
    comment = forms.CharField(label='', required=True ,widget=forms.Textarea(input_attrs[2]))
