# Generated by Django 3.2.7 on 2021-10-27 20:01

from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('replyforum', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='replyforum',
            name='author',
            field=models.ForeignKey(default=None, on_delete=models.SET('Deleted User'), to=settings.AUTH_USER_MODEL),
        ),
    ]
