# Generated by Django 3.2.7 on 2021-11-05 02:04

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('replyforum', '0006_alter_replyforum_author'),
    ]

    operations = [
        migrations.RenameField(
            model_name='replyforum',
            old_name='author',
            new_name='user',
        ),
    ]
